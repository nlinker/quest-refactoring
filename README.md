# Refactoring Quest

## Overview

This collection of little projects takes it's beginning 
from https://github.com/snahider/Refactoring-Golf
and setups the quest for TDD newcomers.

The task is move from stage to stage using elementary refactorings with the 
least possible editing actions. The score is calculated according the following rules:

### General

Score|Description
----|-----
+1	|Each refactoring
+1	|Copy and Paste
+1	|Any shortcut for editing code
+0	|Formatting or deleting blank lines
+0	|Change access methods or classes
+0	|Selections or other actions that do not touch the code
+0	|Optimizing imports

### Penalties

Score|Description
----|-----
+2	|Each manually modified line
*2	|Each change until it compiles again

